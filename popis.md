# Popis algoritmu

1. Na začátku je načten JSON se zadáním hracího světa.
2. Svět je parsován JSON parserem do C# objektu.
3. Je vytvořen objekt typu AstroantsSolver, objektu jsou předány tři parametry: jednorozměrné pole s definicí oblastí světa, počáteční pozice a koncová pozice. AstroantsSolver očekává světy čtvercového tvaru, šířka světa je odvozena podle počtu prvků v poli definujícím oblasti světa.
4. Po vytvoření objektu je zavolána metoda FindShortestWay, která nalezne nejkratší cestu ze startu do cíle v zadaném světě. (Cesta je hledána pomocí modifikace dijkstrova algoritmu)
    1. Na začátku je vytvořena cesta, obsahující pouze výchozí bod, tato cesta je přidána do haldy (Objekt typu cesta (Path) implementuje rozhraní IComparable, a je řazené podle délky cesty)
    2. Dokud jsou v haldě nějaké cesty tak se provádí následující sekvence:
        1. Z haldy se odebere cesta s nejkratší délkou
        2. Najdou se všechny sousední vrcholy posledního vrcholu v cestě
        3. Každý z nalezených sousedních vrcholů se zkontroluje, zda již není přidán v jiné ceste (množina zpracovaných vrcholů). Pokud vrchol je je již v nějaké cestě, tak je "zahozen". Pokud není mezi zpracovanými vrcholy je připraven pro další zpracování.
            1. Pokud nezbyde žádný již nezpracovaný vrchol došla cesta do slepého konce -> v tom případě je zahozena a pokračuje se další nejkratší cestou.
            2. Pokud existuje 1 a více nezpracovaných vrcholů jsou postupně vytvořeny potřebné kopie cest, do kterých je nový vrchol přidán, vrchol je taky přidán do množiny zpracovaných vrcholů
            3. Při přidávání vrcholu do cesty se zkontroluje, zda se nejedná o vrchol ve kterém je umístěn konec. Pokud se jedná o konečný vrchol je vrácena daná cesta jako nejkratší možná cesta.
            4. Pokud přidaný vrchol nebyl konec, je cesta přidána do haldy pro zpracování v dalším cyklu.
    3. Nejkratší cesta (pokud je nalezena) je vrácena přímo z cyklu pomocí return. Pokud neexistuje žádná cesta do cíle je vrácena hodnota null.