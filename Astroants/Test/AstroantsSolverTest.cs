﻿using System.IO;
using System.Net.Http;
using System.Text;
using Astroants.DataStructures.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;

namespace Astroants.Test
{
    [TestFixture]
    public class AstroantsSolverTest
    {
        [Test]
        public void SmallMapInVariableTest()
        {
            const string worldData = "{\r\n  \"id\": \"2727\",\r\n  \"startedTimestamp\": 1503929807498,\r\n  \"map\": {\r\n    \"areas\": [ \"5-R\", \"1-RDL\", \"10-DL\", \"2-RD\", \"1-UL\", \"1-UD\", \"2-RU\", \"1-RL\", \"2-UL\" ]\r\n  },\r\n  \"astroants\": {\"x\": 1, \"y\": 0 },\r\n  \"sugar\": { \"x\": 2, \"y\": 1 }\r\n}";
            const string expectedResult = "DLDRRU";
            var world = JsonConvert.DeserializeObject<World>(worldData);
            var solver = new AstroantsSolver(world.Map.Areas, world.Astroants, world.Sugar);
            var result = solver.FindShortestWay();
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void SmallMapFromFileTest()
        {
            var world = JsonConvert.DeserializeObject<World>(File.ReadAllText("task0.json"));
            const string expectedResult = "DLDRRU";
            var solver = new AstroantsSolver(world.Map.Areas, world.Astroants, world.Sugar);
            var result = solver.FindShortestWay();
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void MapFromFileTest()
        {
            var world = JsonConvert.DeserializeObject<World>(File.ReadAllText("task.json"));
            const string expectedResult = "RDRDDRRDRRRRDDRRDRRURDDRRDRRUURUURRURRRRRUUURRDRDRRRRRDDDDDDDDDDLLLLDDLDDDDLDLDDRDDDRDDRDLDDLLDLDLLLLLDDDDDDLDDDDDLDDDDDDLDDDDDDDDDDLDDDDRDDDRDDDDLDDDDDDRRDDRRDDDDDDDDDDDDRDDDDLLDDDDDDDLDDLLDDDDRRDDDRRDDDDDDRDRDDDDDRDRDDDDRRRRRDRDDDDRDDLDDDDDLLDDLDDDDDDLDLDDDDLDDDDDLDDDDDDDRDDLLLDDDDLDDLLDLLDDDDDDDDDDDDDLDDDDDDLLDLDDLDLLLDLLLDDDLLDDDDDDDLLDDLDDDLDDDDDDDDDDDDDDDDDDDDDDDDDDDRDDRDDRRRRRDDDDRDDDRDRDDDDRRDRRDDDRDDRDDLDDDDDDDDDLDDDDRDDDDDRDDDLDDDDRRRRDRDD";
            var solver = new AstroantsSolver(world.Map.Areas, world.Astroants, world.Sugar);
            var result = solver.FindShortestWay();
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void WebApiTest()
        {
            var httpClient = new System.Net.Http.HttpClient();
            var worldData = httpClient.GetStringAsync("http://tasks-rad.quadient.com:8080/task").Result;
            var world = JsonConvert.DeserializeObject<World>(worldData);
            var solver = new AstroantsSolver(world.Map.Areas, world.Astroants, world.Sugar);
            var solution = solver.FindShortestWay();
            var settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            var jsonResult = new Result { Path = solution };
            var resultObject = JsonConvert.SerializeObject(jsonResult, settings);
            var content = new StringContent(resultObject, Encoding.UTF8, "application/json");
            var apiResult = httpClient.PutAsync($"http://tasks-rad.quadient.com:8080/task/{world.Id}", content).Result;
            var apiResultMessage = JsonConvert.DeserializeObject<ServerResponse>(apiResult.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(true, apiResultMessage.Valid);
            Assert.AreEqual(true, apiResultMessage.InTime);
        }

        [Test]
        public void ImpossibleMap()
        {
            var world = JsonConvert.DeserializeObject<World>(File.ReadAllText("taskImpossible.json"));
            var solver = new AstroantsSolver(world.Map.Areas, world.Astroants, world.Sugar);
            var result = solver.FindShortestWay();
            Assert.AreEqual(null, result);
        }
    }
}
