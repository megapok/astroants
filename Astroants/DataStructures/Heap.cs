﻿using System;
using System.Collections.Generic;

namespace Astroants.DataStructures
{
    class Heap<T> where T : class, IComparable
    {
        private readonly T[] _items;
        private int _size;
        private readonly int _maxSize;

        public int Count => _size;

        public bool IsEmpty => _size == 0;

        public Heap(int maxSize)
        {
            _maxSize = maxSize;
            _items = new T[maxSize + 1];
            _size = 0;
        }

        public void Add(T value)
        {
            var i = ++_size;
            while (value.CompareTo(_items[i / 2]) < 0 && i != 0)
            {
                _items[i] = _items[i / 2];
                i /= 2;
            }
            _items[i] = value;
        }

        public void Add(IEnumerable<T> values)
        {
            foreach (var value in values)
            {
                Add(value);
            }
        }

        public T Remove()
        {
            if (_size == 0) return null;
            T result = _items[1];
            _items[1] = _items[_size--];
            Repair();
            return result;
        }

        private void Repair()
        {
            var top = 1;
            var root = _items[top];
            var next = top * 2;
            if (next < _size && _items[next].CompareTo(_items[next + 1]) > 0)
            {
                next++;
            }
            while (next <= _size && root.CompareTo(_items[next]) > 0)
            {
                _items[top] = _items[next];
                top = next;
                next *= 2;
                if (next < _size && _items[next].CompareTo(_items[next + 1]) > 0)
                {
                    next++;
                }
            }
            _items[top] = root;
        }
    }
}