﻿namespace Astroants.DataStructures.Json
{
    public class ServerResponse
    {
        public bool Valid { get; set; }
        public bool InTime { get; set; }
        public string Message { get; set; }
    }
}
