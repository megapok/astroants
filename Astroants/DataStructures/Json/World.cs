﻿namespace Astroants.DataStructures.Json
{
    class World
    {
        public int Id { get; set; }
        public long StartedTimestamp { get; set; }
        public Map Map { get; set; }
        public Point Astroants { get; set; }
        public Point Sugar { get; set; }
    }
}
