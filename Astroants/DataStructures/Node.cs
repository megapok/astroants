﻿namespace Astroants.DataStructures
{
    class Node
    {
        public Direction Directions { get; set; }
        public byte Coast { get; set; }
        public Point Location { get; set; }
    }
}
