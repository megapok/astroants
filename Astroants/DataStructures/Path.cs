﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Astroants.DataStructures
{
    class Path : IComparable
    {
        private readonly List<Node> _path;
        public int Coast { get; private set; }

        public Node LastNode => _path[_path.Count - 1];

        public Path()
        {
            _path = new List<Node>(600);
        }

        public void AddNode(Node node)
        {
            _path.Add(node);
            Coast += node.Coast;
        }

        public Path Copy()
        {
            var path = new Path();
            foreach (var node in _path)
            {
                path.AddNode(node);
            }
            return path;
        }

        public bool Contains(Node node)
        {
            return _path.Contains(node);
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            if (!(obj is Path otherPath))
            {
                throw new ArgumentException("Object is not a Path");
            }
            return Coast.CompareTo(otherPath.Coast);
        }

        public override string ToString()
        {
            var resultBuilder = new StringBuilder();
            for (var i = 1; i < _path.Count; i++)
            {
                switch (GetDirection(_path[i - 1].Location, _path[i].Location))
                {
                    case Direction.Left:
                        resultBuilder.Append('L');
                        break;
                    case Direction.Up:
                        resultBuilder.Append('U');
                        break;
                    case Direction.Down:
                        resultBuilder.Append('D');
                        break;
                    case Direction.Right:
                        resultBuilder.Append('R');
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            return resultBuilder.ToString();
        }

        private Direction GetDirection(Point startPoint, Point nextPoint)
        {
            if (startPoint.X < nextPoint.X) return Direction.Right;
            if (startPoint.X > nextPoint.X) return Direction.Left;
            if (startPoint.Y < nextPoint.Y) return Direction.Down;
            // if (startPoint.Y > nextPoint.Y) return Direction.Up;
            return Direction.Up;
        }
    }
}
