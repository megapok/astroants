﻿using System;

namespace Astroants.DataStructures
{
    [Flags]
    public enum Direction
    {
        Left = 1,
        Up = 2,
        Down = 4,
        Right = 8
    }
}
