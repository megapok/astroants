﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using Astroants.DataStructures.Json;
using CommandLine;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Astroants
{
    class Program
    {
        public const int WorldWidth = 300;

        static int Main(string[] args)
        {
            var start = DateTime.UtcNow;
            var optionsResult = Parser.Default.ParseArguments<Options>(args);
            if (!(optionsResult is Parsed<Options>))
            {
                return 1;
            }
            var options = ((Parsed<Options>) optionsResult).Value;
            if (options.File != null)
            {
                return RunAgainstFile(options.File, options.WriteTime, start);
            }
            if (options.WebApi)
            {
                return RunAgainstWebApi(options.WriteTime, start);
            }
            Console.WriteLine("Invalid arguments configuration! See --help for more info");
            return 1;
        }

        public static int RunAgainstFile(string file, bool writeTime, DateTime start)
        {
            var world = JsonConvert.DeserializeObject<World>(File.ReadAllText("task.json"));
            var result = ComputeResult(world);
            Console.WriteLine(result);
            if (writeTime)
            {
                var end = DateTime.UtcNow;
                Console.WriteLine("Program was done in: " + (end - start));
            }
            return 0;
        }

        public static int RunAgainstWebApi(bool writeTime, DateTime start)
        {
            var httpClient = new System.Net.Http.HttpClient();
            var data = httpClient.GetStringAsync("http://tasks-rad.quadient.com:8080/task").Result;
            var world = JsonConvert.DeserializeObject<World>(data);
            var result = ComputeResult(world);
            var content = new StringContent(result,Encoding.UTF8, "application/json");
            var apiResult = httpClient.PutAsync($"http://tasks-rad.quadient.com:8080/task/{world.Id}", content).Result;
            var apiResultMessage = apiResult.Content.ReadAsStringAsync().Result;
            Console.WriteLine(result);
            Console.WriteLine(apiResultMessage);
            if (writeTime)
            {
                var end = DateTime.UtcNow;
                Console.WriteLine("Program was done in: " + (end - start));
            }
            return 0;
        }

        public static string ComputeResult(World world)
        {
            var solver = new AstroantsSolver(world.Map.Areas, world.Astroants, world.Sugar);
            var solution = solver.FindShortestWay();
            var settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            var result = new Result { Path = solution };
            return JsonConvert.SerializeObject(result, settings);
        }
    }
}