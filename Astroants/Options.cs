﻿using CommandLine;

namespace Astroants
{
    class Options
    {
        [Option('t', "writetime", Required = false, HelpText = "Write time which was taken from start to end")]
        public bool WriteTime { get; set; }

        [Option('f', "file", Required = false, HelpText = "Name or path to file which will be loaded to compute result")]
        public string File { get; set; }

        [Option('w', "webapi", Default = true, Required = false, HelpText = "Run program against web api, if no file is specified")]
        public bool WebApi { get; set; }
    }
}
