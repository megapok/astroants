﻿using System;
using System.Collections.Generic;
using Astroants.DataStructures;

namespace Astroants
{
    class AstroantsSolver
    {
        private readonly Point _start;
        private readonly Point _end;
        private readonly int _mapWidth;
        private readonly Node[] _nodes;

        public AstroantsSolver(string[] mapAreas, Point start, Point end)
        {
            _start = start;
            _end = end;
            _mapWidth = (int) Math.Sqrt(mapAreas.Length);
            _nodes = ParseMapAreas(mapAreas);
        }

        private Node[] ParseMapAreas(string[] mapAreas)
        {
            var nodes = new Node[mapAreas.Length];
            for (var i = 0; i < mapAreas.Length; i++)
            {
                nodes[i] = ParseArea(mapAreas[i]);
                nodes[i].Location = GetLocationPointFromArrayIndex(i);
            }
            return nodes;
        }

        private Node ParseArea(string area)
        {
            var node = new Node();
            var values = area.Split('-');
            node.Coast = Convert.ToByte(values[0]);
            if (values[1].Contains("L"))
            {
                node.Directions = node.Directions | Direction.Left;
            }
            if (values[1].Contains("U"))
            {
                node.Directions = node.Directions | Direction.Up;
            }
            if (values[1].Contains("D"))
            {
                node.Directions = node.Directions | Direction.Down;
            }
            if (values[1].Contains("R"))
            {
                node.Directions = node.Directions | Direction.Right;
            }
            return node;
        }

        private Point GetLocationPointFromArrayIndex(int index)
        {
            return new Point(index % _mapWidth, index / _mapWidth);
        }

        public string FindShortestWay()
        {
            var startNode = _nodes[GetArrayIndex(_start)];
            var paths = new Heap<Path>(_mapWidth * _mapWidth);
            var startPath = new Path();
            startPath.AddNode(startNode);
            paths.Add(startPath);
            var proccessedNodes = new HashSet<Node> { startNode };
            while (!paths.IsEmpty)
            {
                var path = paths.Remove();
                var neighbors = GetNeighborNodes(path.LastNode);
                var nodesToAdd = new List<Node>(3);
                foreach (var neighbor in neighbors)
                {
                    if (proccessedNodes.Contains(neighbor))
                        continue;
                    nodesToAdd.Add(neighbor);
                    proccessedNodes.Add(neighbor);
                }
                if (nodesToAdd.Count == 0)
                    continue;
                for (var i = 0; i < nodesToAdd.Count; i++)
                {
                    var pathToAdd = (i == nodesToAdd.Count - 1) ? path : path.Copy();
                    pathToAdd.AddNode(nodesToAdd[i]);
                    if (IsPathToEnd(pathToAdd))
                        return pathToAdd.ToString();
                    paths.Add(pathToAdd);
                }
            }
            return null;
        }

        private bool IsPathToEnd(Path path)
        {
            return path.LastNode.Location.Equals(_end);
        }

        private Node[] GetNeighborNodes(Node node)
        {
            var nodes = new List<Node>(4);
            if (node.Directions.HasFlag(Direction.Left))
            {
                nodes.Add(_nodes[GetArrayIndex(node.Location.X - 1, node.Location.Y)]);
            }
            if (node.Directions.HasFlag(Direction.Right))
            {
                nodes.Add(_nodes[GetArrayIndex(node.Location.X + 1, node.Location.Y)]);
            }
            if (node.Directions.HasFlag(Direction.Up))
            {
                nodes.Add(_nodes[GetArrayIndex(node.Location.X, node.Location.Y - 1)]);
            }
            if (node.Directions.HasFlag(Direction.Down))
            {
                nodes.Add(_nodes[GetArrayIndex(node.Location.X, node.Location.Y + 1)]);
            }
            return nodes.ToArray();
        }

        private int GetArrayIndex(Point point)
        {
            return GetArrayIndex(point.X, point.Y);
        }

        private int GetArrayIndex(int x, int y)
        {
            return x + y * _mapWidth;
        }
    }
}
