# Astroants

Řešení Quadient souteže astroants. Zadání soutěže lze najít [zde](./Astroants/quadient-soutez.pdf).

## CLI

Jedná se o konzolovou aplikaci napsonou v .NET Core.

Aplikaci lze spustit s následujícími parametry:

Parametr | Popis
-------- | ----
--help   | Výpis nápovědy o použití programu
--file [název souboru] | spustí program a načte data ze zadaného souboru
--webapi | spustí program a načte data z webu, výsledek pošle na web a vypíše výsledek.
--writetime | vypíše čas trvání programu, včetně načítání dat a výpočet algoritmu

Bez zadaného parametru se program spustí proti Webovému API, pokud je zadaný parametr file program se pokusí zpracovat pouze tento soubor.

## Popis funkcionality

Popis funkcionality lze najít [zde](./popis.md)

## Autor

Autorem programu je Miroslav Pokorný
